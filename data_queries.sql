/* QUERY DESCRIPTION
  NUMBER OF ACTIVE HOSPITAL DAYS
*/
SELECT COUNT (*) AS ACTIVE_DAYS
FROM (SELECT DISTINCT strftime('%Y %m %d', arriving_time) as DAYS
	  FROM resource_consumption AS DISTINCT_DAYS);

/* QUERY DESCRIPTION
  THE TOTAL LOS OF EACH DEPARTMENT
*/
SELECT resource_name,SUM(CAST((
    JULIANDAY(end_time) - JULIANDAY(arriving_time)) * 24 * 60 * 60 AS REAL))/86400 AS DELTA_T
FROM resource_consumption
GROUP BY resource_name;

/* QUERY DESCRIPTION
  THE MIN LOS OF EACH DEPARTMENT
*/
SELECT resource_name,MIN(CAST((
    JULIANDAY(end_time) - JULIANDAY(arriving_time)) * 24 * 60 * 60 AS REAL))/86400 AS DELTA_T
FROM resource_consumption
GROUP BY resource_name;

/* QUERY DESCRIPTION
  THE MAX LOS OF EACH DEPARTMENT
*/

SELECT resource_name,MAX(DELTA_SUM) as MAX_SUM
FROM (       select patient_index, resource_name, SUM(DELTA_T) AS DELTA_SUM
             FROM (     select patient_index, resource_name,
                        CAST((JULIANDAY(end_time) - JULIANDAY(arriving_time))* 24 * 60 * 60 AS REAL)/86400 AS DELTA_T
                        FROM resource_consumption)
              GROUP BY patient_index ,resource_name)
GROUP BY resource_name;

/* QUERY DESCRIPTION
  THE AVG LOS OF EACH DEPARTMENT
*/
SELECT resource_name,AVG(DELTA_SUM) as AVG_SUM
FROM (       select patient_index, resource_name, SUM(DELTA_T) AS DELTA_SUM
             FROM (     select patient_index, resource_name,
                        CAST((JULIANDAY(end_time) - JULIANDAY(arriving_time))* 24 * 60 * 60 AS REAL)/86400 AS DELTA_T
                        FROM resource_consumption)
              GROUP BY patient_index ,resource_name)
GROUP BY resource_name;
/* QUERY DESCRIPTION
  THE TOTAL NUMBER OF PATIENTS CONSUMPTION ON SPECIFIC DAY IN THE TESTED MONTH
*/
SELECT strftime('%d', arriving_time) AS ARRIVAL_DAY,COUNT(*)
FROM resource_consumption
WHERE resource_index == 0
GROUP BY ARRIVAL_DAY;

/* QUERY DESCRIPTION
  THE TOTAL NUMBER OF PATIENTS IN EACH SIMULATED DAY PER DEPARTMENT
*/
SELECT resource_name,strftime('%Y %m %d', arriving_time) AS ARRIVAL_DAY,COUNT(*) PATEIENTS_COUNT
FROM resource_consumption
GROUP BY resource_name,ARRIVAL_DAY ORDER BY ARRIVAL_DAY;

/* QUERY DESCRIPTION
  THE TOTAL NUMBER OF PATIENTS IN EACH SIMULATED DAY HOSPITAL WISE
*/
SELECT strftime('%Y-%m-%d', arriving_time) AS ARRIVAL_DAY,COUNT(*) PATIENTS_COUNT
FROM resource_consumption
GROUP BY ARRIVAL_DAY;

/* QUERY DESCRIPTION
  THE MAX NUMBER OF PATIENTS IN EACH SIMULATED DAY PER DEPARTMENT
*/
SELECT resource_name,ARRIVAL_DAY,MAX(PATEIENTS_COUNT)
FROM (SELECT resource_name,strftime('%Y %m %d', arriving_time) AS ARRIVAL_DAY,COUNT(*) PATEIENTS_COUNT
      FROM resource_consumption
      GROUP BY resource_name,ARRIVAL_DAY) AS PED
GROUP BY resource_name;

/* QUERY DESCRIPTION
  THE MIN NUMBER OF PATIENTS IN EACH SIMULATED DAY PER DEPARTMENT
*/
SELECT resource_name,ARRIVAL_DAY,MIN(PATEIENTS_COUNT)
FROM (SELECT resource_name,strftime('%Y %m %d', arriving_time) AS ARRIVAL_DAY,COUNT(*) PATEIENTS_COUNT
      FROM resource_consumption
      GROUP BY resource_name,ARRIVAL_DAY) AS PED
GROUP BY resource_name;

/* QUERY DESCRIPTION
  THE AVG NUMBER OF PATIENTS PER DEPARTMENT
*/
SELECT resource_name,AVG(PATEIENTS_COUNT)
FROM (SELECT resource_name,strftime('%Y %m %d', arriving_time) AS ARRIVAL_DAY,COUNT(*) PATEIENTS_COUNT
      FROM resource_consumption
      GROUP BY resource_name,ARRIVAL_DAY) AS PED
GROUP BY resource_name;

/* QUERY DESCRIPTION
  AVG ARRIVAL GAP BETWEEN PATIENTS, PER DEPARTMENT - ALL SIMULATION
*/
SELECT resource_name,AVG(ARR_DELTA)/60 AS ARR_RATE
FROM (

  SELECT
    resource_name,
    CAST(((SELECT JULIANDAY(RC_I.arriving_time)
           FROM resource_consumption RC_I
           WHERE (RC_I.arriving_time > RC_O.arriving_time) AND
                 (RC_I.resource_name = RC_O.resource_name) AND
                 NOT EXISTS(SELECT *
                            FROM resource_consumption X
                            WHERE (RC_O.arriving_time < X.arriving_time) AND
                                  (RC_I.resource_name = RC_O.resource_name) AND
                                  (X.resource_name = RC_O.resource_name) AND
                                  (RC_I.arriving_time > X.arriving_time))) - JULIANDAY(arriving_time)) * 24 * 60 * 60 AS
         REAL) AS ARR_DELTA
  FROM resource_consumption RC_O
  WHERE ARR_DELTA IS NOT NULL

) AS ST

GROUP BY resource_name;

/* QUERY DESCRIPTION
  NUMBER OF PATIENTS PER DAY WHO ARRIVED AT CLOSED HOURS
*/
SELECT strftime('%Y-%m', arriving_time) AS HOSPITAL_DATE, COUNT(*) AS INACTIVE_HOUR_CASES
From resource_consumption
WHERE closed_hours IS 1 GROUP BY strftime('%Y-%m', arriving_time);

SELECT resource_name,MAX(CAST((
    JULIANDAY(end_time) - JULIANDAY(arriving_time)) * 24 * 60 * 60 AS REAL))/86400 AS DELTA_T
FROM resource_consumption
GROUP BY resource_name;



SELECT resource_name,MAX(DELTA_SUM)
FROM (       select patient_index, resource_name, SUM(DELTA_T) AS DELTA_SUM
             FROM (     select patient_index, resource_name,
                        CAST((JULIANDAY(end_time) - JULIANDAY(arriving_time))* 24 * 60 * 60 AS REAL)/86400 AS DELTA_T
                        FROM resource_consumption)
              GROUP BY patient_index ,resource_name)
GROUP BY resource_name;








class ResourceConsumption(object):
    def __init__(self, _PatientIndex, _PatientStateIndex, _ResourceIndex, _ResourceName, _ArrivingTime, _BeginningTime,
                 _EndTime, _NotActive):
        self.PatientIndex = _PatientIndex
        self.PatientStateIndex = _PatientStateIndex
        self.ResourceIndex = _ResourceIndex
        self.ResourceName = _ResourceName
        self.ArrivingTime = _ArrivingTime
        self.BeginningTime = _BeginningTime
        self.EndTime = _EndTime
        self.NotActive = _NotActive

import logging
import time

from bin.resource_consumption import ResourceConsumption



class Patient(object):
    def __init__(self, _PatientIndex, _ResManagerInstance,_TimeManager):
        self.PatientIndex = _PatientIndex
        self.ResManagerInstance = _ResManagerInstance
        self.TimeManager = _TimeManager
        self.StateIndex = 0
        self.CurrentResource = None
        self.PatientFlow = []
        self.EndOPeration = self.TimeManager.GetCurrentTime()
        self.module_logger = logging.getLogger('__name__')
        self.module_logger.setLevel(logging.INFO)

    def __call__(self):
        while True:
            self.CurrentResource = self.ResManagerInstance.GetNextResource(self.CurrentResource)
            _ArriveTime = self.EndOPeration
            print('Patient index {0} is on {1} resource'.format(self.PatientIndex, self.CurrentResource.ResourceName))
            _Active = self.CurrentResource.AcquireResource()
            _BeginTime = self.TimeManager.GetCurrentTime()
            _LOS = self.CurrentResource.GetLengthOfStay()
            time.sleep(_LOS)
            print('Patient index {0} consuming {1} resource with LOS {2} '
                  .format(self.PatientIndex, self.CurrentResource.ResourceName, _LOS))
            self.CurrentResource.ReleaseResource()
            _EndTime = self.TimeManager.GetCurrentTime()
            self.EndOPeration = _EndTime
            x = ((_ArriveTime - self.TimeManager.getBaseTime()) * self.TimeManager.GetFactor())
            y = ((_BeginTime - self.TimeManager.getBaseTime()) * self.TimeManager.GetFactor())
            z = ((_EndTime - self.TimeManager.getBaseTime()) * self.TimeManager.GetFactor())

            self.PatientFlow.append(ResourceConsumption(
                self.PatientIndex,
                self.StateIndex,
                self.CurrentResource.ResourceIndex,
                self.CurrentResource.ResourceName,
                _ArriveTime + x,
                _BeginTime + y,
                _EndTime + z,
                _Active
            ))
            self.StateIndex += 1
            if self.CurrentResource.IsFinal():
                break

import json

import numpy

from bin.resource import Resource

RES_CONFIG_JSON_PATH = 'resources.json'
RES_BEGIN_INDEX = 0


class ResourceManager(object):
    def __init__(self, _TimeManager):
        self.Resources = []
        self.TimeManager = _TimeManager
        with open(RES_CONFIG_JSON_PATH) as _FileContent:
            _ResConfJson = json.load(_FileContent)
            self.Resources = list(map(lambda _Res: self.DeSerializeResource(_Res), _ResConfJson))

    def GetNextResource(self, _CurrentResource):
        if None is _CurrentResource:
            return self.Resources[RES_BEGIN_INDEX]
        _ResourcesProb = list(map(lambda _NextRes: _NextRes.ResourceProb, _CurrentResource.NextResources))
        _NextResIndex = numpy.random.choice(numpy.arange(0, len(_ResourcesProb)), p=_ResourcesProb)
        return self.Resources[_CurrentResource.NextResources[_NextResIndex].ResourceIndex]

    def DeSerializeResource(self, _ResDictionary):
        _NextResources = list(map(lambda _NextResDict: Resource.NextResource(
            _NextResDict['ResourceIndex'],
            _NextResDict['ResourceProb']
        ), _ResDictionary['NextResources'])) if None is not _ResDictionary['NextResources'] else None
        return Resource(
            _ResDictionary['ResourceIndex'],
            _ResDictionary['ResourceName'],
            _ResDictionary['IsFinalResource'],
            _ResDictionary['InstanceCounter'],
            _ResDictionary['LengthOfStayMin'],
            _ResDictionary['LengthOfStayMax'],
            _ResDictionary['OpenHour'],
            _ResDictionary['CloseHour'],
            _NextResources,
            self.TimeManager
        )

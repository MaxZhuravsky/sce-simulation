import datetime

class TimeManager(object):

    def __init__(self,_TimeFactor):
        self.BaseTime = datetime.datetime.now()
        self.TimeFactor = _TimeFactor

    def GetCurrentTime(self):
        return self.BaseTime + (datetime.datetime.now() - self.BaseTime)

    def GetFactor(self):
        return self.TimeFactor

    def getBaseTime(self):
        return self.BaseTime

    def GetElapsedDays(self):
        delta = (datetime.datetime.now() - self.BaseTime)
        return delta.seconds  # each real second is a sim day

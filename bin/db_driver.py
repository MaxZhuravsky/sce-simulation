import sqlite3



class DbDriver(object):
    def __init__(self):
        self.connection = sqlite3.connect('simulation1000t.db')
        self.cursor = self.connection.cursor()

    def createTable(self):
        self.cursor.execute('''DROP TABLE IF EXISTS resource_consumption''')
        self.cursor.execute('''CREATE TABLE resource_consumption
                     (patient_index NUMBER , patient_state_index NUMBER, resource_index NUMBER , resource_name text, 
                     arriving_time DATETIME ,beginning_time DATETIME, end_time DATETIME, closed_hours NUMERIC )''')
    def insertData(self, _PatientsContainer):
        for patient in _PatientsContainer:

            for flow in patient.PatientFlow:
                self.cursor.execute('''INSERT INTO resource_consumption(patient_index, patient_state_index, resource_index, resource_name, arriving_time, beginning_time , end_time, closed_hours) VALUES(?,?,?,?,?,?,?,?)''',
                (patient.PatientIndex,flow.PatientStateIndex , flow.ResourceIndex, flow.ResourceName, flow.ArrivingTime, flow.BeginningTime, flow.EndTime, flow.NotActive))

        self.connection.commit()
import random
import threading

import time


class Resource(object):
    class NextResource:
        def __init__(self, _ResourceIndex, _ResourceProb):
            self.ResourceIndex = _ResourceIndex
            self.ResourceProb = _ResourceProb

    def __init__(self, _ResourceIndex, _ResourceName, _IsFinalResource, _InstanceCounter, _LengthOfStayMax,
                 _LengthOfStayMin, _OpenHour, _CloseHour,  _NextResources, _TimeManager ):
        self.ResourceIndex = _ResourceIndex
        self.ResourceName = _ResourceName
        self.IsFinalResource = _IsFinalResource
        self.ResourceSemaphore = threading.Semaphore(_InstanceCounter)
        self.LengthOfStayMax = _LengthOfStayMax
        self.LengthOfStayMin = _LengthOfStayMin
        self.NextResources = _NextResources
        self.OpenHour = _OpenHour
        self.CloseHour = _CloseHour
        self.TimeManager = _TimeManager

    def IsFinal(self):
        return self.IsFinalResource

    def GetLengthOfStay(self):
        return random.uniform(self.LengthOfStayMax, self.LengthOfStayMin)

    def AcquireResource(self):
        _ActualCurrentTime = self.TimeManager.GetCurrentTime()
        _CurrTime = _ActualCurrentTime + ((_ActualCurrentTime - self.TimeManager.getBaseTime()) * self.TimeManager.GetFactor())
        _IsLocked = False
        if self.OpenHour != -1 and self.CloseHour != -1:
            if _CurrTime.hour < self.OpenHour or _CurrTime.hour > self.CloseHour:
                _SleepTime = ((self.OpenHour - _CurrTime.hour) % 24) / 24
                time.sleep(_SleepTime)
                _IsLocked = True

        self.ResourceSemaphore.acquire()
        return _IsLocked

    def ReleaseResource(self):
        self.ResourceSemaphore.release()

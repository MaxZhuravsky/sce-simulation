import threading
import time

import numpy

from bin.db_driver import DbDriver
from bin.patient import Patient
from bin.resource_manager import ResourceManager
from bin.time_manager import TimeManager

POISSON_LAMBDA = 64
POISSON_RES_DIVIDER = 100
TIME_FACTOR = 86400
GAP_1 = 0.1736  # 250m
GAP_2 = 0.3472  # 500m
GAP_3 = 0.6944  # 1000m
GAP_4 = 1.0416  # 1500m


def main():
    _TimeManagerInstance = TimeManager(_TimeFactor = TIME_FACTOR)
    _ResourceManagerInstance = ResourceManager(_TimeManagerInstance)
    _PatientsContainer = []
    _ThreadsContainer = []
    _ArrivalGap = GAP_3
    i = 0
    while _TimeManagerInstance.GetElapsedDays() < 360:
        _PatientTmp = Patient(i, _ResourceManagerInstance,_TimeManagerInstance)
        _PatientsContainer.append(_PatientTmp)
        _ThreadTmp = threading.Thread(target = _PatientTmp)
        _ThreadsContainer.append(_ThreadTmp)
        _ThreadTmp.start()
        if _ArrivalGap is None:
            gap = numpy.random.poisson(lam = POISSON_LAMBDA)/POISSON_RES_DIVIDER
            print(gap)
            time.sleep(gap)
        else:
            time.sleep(_ArrivalGap)
        i += 1
    print(_TimeManagerInstance.GetElapsedDays())
    for _Thread in _ThreadsContainer:
        _Thread.join()
    dbDriver = DbDriver()
    dbDriver.createTable()
    dbDriver.insertData(_PatientsContainer)
if __name__ == '__main__':
    main()